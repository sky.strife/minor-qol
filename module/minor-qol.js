"use strict";

import AbilityTemplate from "../../../systems/dnd5e/module/pixi/ability-template.js";
import { SpellLevelDialog } from "../apps/SpellLevelDialog.js"
import { addChatMessageContextOptions } from "../../../systems/dnd5e/module/chat.js";
var knownSheets = undefined;

// Strings to look for in chatmessages
var attackRollFlavor;
var damageRollFlavor;
var savingThrowText;
var undoDamageText
// Spells that do no damage when saved against.
let noDamageSaves = [];

let debug = false;
let log = (...args) => console.log("Minor Qol | ", ...args);

const MESSAGETYPES = {
    hitData: 1,
    saveData: 2
};

var minorQolStateData = {};
function resetStatus(versatile=false) {
  minorQolStateData.verstatile = versatile;
  minorQolStateData.hitTargets = game.user.targets; 
  minorQolStateData.isCrtical = false; 
  minorQolStateData.isFumble = false; 
  minorQolStateData.saves = new Set(); 
  minorQolStateData.versatile = versatile;
  minorQolStateData.targetDispositions = [-1, 0, 1];
  minorQolStateData.speaker = ChatMessage.getSpeaker();
}

let hotbarHandler = (bar, data, slot) => {
  if(debug) log("Hotbar Hook fired", bar, data, slot);
  if (!macroSpeedRolls) return true;
  if (data.type !== "Item") return true;
  if (debug) log("hotbar drop", "Creating macro");
  createMinorQOLMacro(data.data, slot);
  return false;
}

export function initSetup() {
    // get all the game.settings.("minor-qol", ...)
    fetchParams();
    // Watch for minor-qol hidden names and reveal them to the GM
    setupHiddenNameButtons();
      // when fixed replace with Hooks.on("hotbarDrop", hotbarHandler)
    Hooks._hooks.hotbarDrop = [hotbarHandler].concat(Hooks._hooks.hotbarDrop || []);
}

export function readySetup() {
  minorQolStateData.hideSaves = false;
  if (debug) log("Setup entered")
  if (!knownSheets) {
      console.error("Minor-qol | known sheets not setup - module not installed");
      return;
  }
  for (let sheetName of Object.keys(knownSheets)) {
      Hooks.on("render" + sheetName, enableSheetQOL);
  }
  Hooks.on("renderedAlt5eSheet", enableSheetQOL);
  Hooks.on("renderedTidy5eSheet", enableSheetQOL);

  // When a measured template is display auto target actors it auto target set
  Hooks.on("createMeasuredTemplate", selectTargets);
  // respond to chat rolls (attack and damage trigger further actions)
  setupRollHandling();

  // need to wait for translations to be loaded before setting these
  attackRollFlavor = i18n("minor-qol.attackRollFlavorText");
  damageRollFlavor = i18n("minor-qol.damageRollFlavorText");
  savingThrowText = i18n("minor-qol.savingThrowText");
  undoDamageText = i18n("minor-qol.undoDamageFrom");

  var prevCombatant = "";
  // setup for removing targets at end of turn.
  Hooks.on("updateCombat", (combat, update, options, user) => {
    if (autoRemoveTargets !== "all") return;
    if (prevCombatant === "" && !game.user.isGM) { // have not seen any yet
      prevCombatant = combat.current.tokenId;
      return;
    } else {
      if (game.user.isGM || canvas.tokens.controlled.map(t=>t.id).includes(prevCombatant)) {
        removeAllTargets()
      }
      prevCombatant = combat.current.tokenId;
      return;
    }
  })
}

let itemRollButtons, speedItemRolls, autoTarget;
let autoCheckHit, autoCheckSaves, autoRollDamage;
let addChatDamageButtons, autoApplyDamage, damageImmunities;
let macroSpeedRolls, hideNPCNames, itemDeleteCheck, nsaFlag;
let autoItemEffects, coloredBorders, rangeTarget, autoRemoveTargets;
let checkBetterRolls, playerRollSaves, playerSaveTimeout;
let saveRequests = {};

export let fetchParams = () => {
  itemRollButtons = game.settings.get("minor-qol", "ItemRollButtons");
  speedItemRolls = game.settings.get("minor-qol", "SpeedItemRolls");
  autoTarget = game.settings.get("minor-qol", "AutoTarget");
  autoCheckHit = game.settings.get("minor-qol", "AutoCheckHit");
  autoRemoveTargets = game.settings.get("minor-qol", "AutoRemoveTargets");
  autoCheckSaves = game.settings.get("minor-qol", "AutoCheckSaves");
  autoRollDamage = game.settings.get("minor-qol", "AutoRollDamage");
  addChatDamageButtons = game.settings.get("minor-qol", "AddChatDamageButtons");
  autoApplyDamage = game.settings.get("minor-qol", "AutoApplyDamage");
  damageImmunities = game.settings.get("minor-qol", "DamageImmunities");
  macroSpeedRolls = game.settings.get("minor-qol", "MacroSpeedRolls");
  hideNPCNames = game.settings.get("minor-qol", "HideNPCNames");
  itemDeleteCheck = game.settings.get("minor-qol", "ItemDeleteCheck");
  nsaFlag = game.settings.get("minor-qol", "showGM");
  autoItemEffects = game.settings.get("minor-qol", "AutoEffects");
  coloredBorders = game.settings.get("minor-qol", "ColoredBorders");
  rangeTarget = game.settings.get("minor-qol", "RangeTarget");
  playerRollSaves = game.settings.get("minor-qol", "PlayerRollSaves")
  playerSaveTimeout = game.settings.get("minor-qol", "PlayerSaveTimeout")
  if (betterRollsActive) {
    checkBetterRolls = game.settings.get("betterrolls5e", "d20Mode") === 1;
  }
}

let processSecretMessage = (data, options) => {
  if (!data.whisper  || data.whisper.length === 0) return true;
  let gmIds = ChatMessage.getWhisperRecipients("GM");
  gmIds = gmIds.filter(id => !data.whisper.includes(id));
  data.whisper = data.whisper.concat(gmIds);
  return true;
}
/**
 * If we are auto checking saves disable the display of created saves - checkSaves will display a combo card instead
 * @param {[message]} messages 
 * @param {} data 
 * @param {} options 
 */
let processpreCreateSaveRoll = (data, options) => {
  if (!minorQolStateData.hideSaves || !autoCheckSaves) {
    minorQolStateData.saveCount = 0;
    minorQolStateData.hideSaves = false;
    return true;
  }
  if (data.user !== game.user.id) return true;
  options.displaySheet = false;
  minorQolStateData.saveCount -= 1;
  minorQolStateData.hideSaves = minorQolStateData.saveCount > 0;
  return false;
}

let processPreCreateDamageRoll = (data, options) => {
  if (debug) log("processpreCreateDamageRoll", data, options);
  // if (data.user != game.user._id) return; not required only creator gets this message
  console.warn("process pre create ", data, options);
  let actor = game.actors.tokens[data.speaker.token];
  if (!actor) actor = game.actors.get(data.speaker.actor);
  let item = actor.items.find(i => data.flavor.startsWith(`${i.name}${damageRollFlavor}`) && i.hasDamage);
  //if (!item) item = minorQolStateData.item; // use the stored copy if we could not find it.
  return true;
}
let processBetterRollsChatCard = (message, html, data) => {
  const userId = message.user.id;
  if (!saveRequests[userId]) return true;
  const title = html.find(".item-name")[0]?.innerHTML
  if (!title) return true;
  if (!title.includes("Save")) return true;
  const formula = "1d20";
  const total = html.find(".dice-total")[0]?.innerHTML;
  if (!saveRequests[userId]) return true;
  saveRequests[userId]({total, formula})
  delete saveRequests[userId];
  return true;
}

let processpreCreateBetterRollsCard = async (data, options) => {
  resetStatus();

  let html = $(data.content);
  let rollDivs = html.find(".dice-roll.red-dual");//.find(".dice-row-item");
  let rollData = html.find("red-full");
  if (debug) log("better rolls ", rollData, rollDivs)

  let itemId = html[0].attributes["data-item-id"];
  if (!itemId) return true; // not an item roll.
  itemId = itemId.nodeValue;
  let itemName = html.find(".item-name")[0].innerHTML;
  let itemRe = /[^(]\(([\d]*)[^)]*\)/

  let actor = game.actors.tokens[data.speaker.token];
  if (!actor) actor = game.actors.get(data.speaker.actor);
  let item = actor.items.get(itemId);
  let levelMatch =  itemName.match(itemRe);
  let itemLevel = levelMatch ? levelMatch[1] : item?.data?.data?.level ? item.data.data.level : 0;
  let isCritical = html[0].attributes["data-critical"].nodeValue === "true";
  let damageStart = 0;
  let attackTotal = -1;
  if (item.hasAttack) {
    damageStart = 1
    const attackRolls = $(rollDivs[0]).find(".dice-total");
    for (let i = 0; i < attackRolls.length; i++) {
      if (!attackRolls[i].classList.value.includes("ignore")) {
        attackTotal = parseInt(attackRolls[i]?.innerHTML);
        break;
      }
    }
  }
  let damageList = [];
  if (debug) log("Better Rolls Chat card", itemName, itemLevel, attackTotal, damageStart, isCritical)

  // We can't process the targets immediately since we might be still placing the spell template
  Hooks.once("minor-qol-targeted", async () => {
    for (let i = damageStart; i < rollDivs.length; i++) {
      let child = rollDivs[i].children;
      let damage = 0;
      for (let j = 0; j < $(child[1]).find(".dice-total")[0].children.length; j++) {
        let damageitem = parseInt($(child[1]).find(".dice-total")[0].children[j].innerHTML);
        if (!isNaN(damageitem)) damage += damageitem;
      }
      let type = child[0].innerHTML.toLowerCase().replace("damage - ", "");
      damageList.push({type, damage})
    }
    let theTargets = game.user.targets;
    let isFumble = false;
    let isHit = true;

    // Assume we have a single die result
    // isCritical = attackRoll.parts[0].results[0] >= attackRoll.parts[0].options.critical;
    if (autoCheckHit !== "none" && item.hasAttack && attackTotal) {
      // let theTargets = game.user.targets;
      let msg = "";
      let sep = "";
      
      // check for a hit/critical/fumble
      isHit = false;
      theTargets = new Set();
      let hitDisplay = [];

      for (let targetToken of game.user.targets) {
        let targetActor = targetToken.actor;
        if (!isFumble && !isCritical) {
            // check to see if the roll hit the target
            let targetAC = targetActor.data.data.attributes.ac.value;
            if (game.user.isGM) log(`${data.speaker.alias} Rolled a ${attackTotal} to hit ${targetActor.name}s AC of ${targetAC}`);
            isHit = attackTotal >= targetAC;
        }
        // Log the hit on the target
        let attackType = item?.name ? i18n(item.name) : "Attack";
        let hitString = isCritical ? i18n("minor-qol.criticals") : isFumble? i18n("minor-qol.fumbles") : isHit ? i18n("minor-qol.hits") : i18n("minor-qol.misses");
        hitDisplay.push({isPC: targetToken.actor.isPC, target: targetToken, hitString, attackType});

        // If we hit and we have targets and we are applying damage say so.
        if (isHit || isCritical) theTargets.add(targetToken);
      }
      let templateData = {
        hits: hitDisplay, 
        isGM: game.user.isGM,
        damageAppliedString: autoApplyDamage !== "none" && theTargets.size > 0 && autoRollDamage !== "none" ? i18n("minor-qol.damage-applied") : ""
      }
      let content = await renderTemplate("modules/minor-qol/templates/hits.html", templateData);
      if (game.user.targets.size > 0) {
        let chatData = {
          user: game.user._id,
          speaker: { actor: actor, alias: actor.name },
          content: content,
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
          flags: { minorQolType: MESSAGETYPES.hitData }
        }
        if (autoCheckHit === "whisper" || ["gmroll", "blindroll"].includes(data.rollMode)) {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM");
          chatData.user = ChatMessage.getWhisperRecipients("GM")[0];
        }
        ChatMessage.create(chatData);
      }
    }
    checkSaves(game.user.targets, item, ["gmroll", "blindroll"].includes(data.rollMode)).then((saves) => {
      if (autoApplyDamage !== "none") {
        let totalDamage = damageList.reduce((acc, a) => a.damage + acc, 0);
        //let theTargets = minorQolStateData.hitTargets;
        //let saves = minorQolStateData.saves;
  
        if (totalDamage > 0) applyTokenDamage(damageList, totalDamage, theTargets, item, saves);
      }
      if (item && autoItemEffects && dynamicEffectsActive && data.user ===  game.user._id && theTargets && theTargets.size > 0) { // perhaps apply item effects
        // if someone saved we want the failedSaves to apply effects if no-one saved then all targets get the effect.
        if (saves.size > 0) theTargets = minorQolStateData.failedSaves;
        // assume effects only applied to hit targets
        if (autoCheckHit !== "none" || (autoCheckSaves !== "none" && theTargets.size > 0 && minorQolStateData.item !== null)) {
          if (debug) log("processDamage Roll - about to call doEffects ", item, item.actor, theTargets)
          let spellLevel = minorQolStateData.spellLevel;
          DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: theTargets, 
                                  whisper: true, spellLevel: itemLevel || 0}) 
        }
      }
      filterTargets();
    })
  })

  // If we are not auto targetting then cause the rest of the processing to fire.
  if (!autoTarget || !item.hasAreaTarget) Hooks.callAll("minor-qol-targeted");
}

let processPreCreateBeyond20Message = (data, options) => {
  return true;
  if (data.user !== game.user.id || !data.content.includes('<div class="beyond20-message">')) return true;
  // extract the attack part and damage part
    //console.log("precreate beyond 20 message ", data, options);
}

/**
 * Set handlers for attack and damage rolls
 */
let setupRollHandling = () => {
  Hooks.on("preCreateChatMessage", (data, options, other) => {
    if (debug) log("preCreateChatMessage hook", data, options);
    if (nsaFlag) processSecretMessage(data,options);
    if (checkBetterRolls && data.content.startsWith('<div class="dnd5e red-full chat-card"'))
    {
      return processpreCreateBetterRollsCard(data, options)
    }
    if (!data || !data.flavor) {
      return true;
    }
    // if (data.content.includes(`div class="beyond20-message">`)) return procsesCreatBeyond20Message(data. options);
    if (data.flavor.includes(attackRollFlavor)) return processPreCreateAttackRoll(data, options);
    if (data.flavor.includes(damageRollFlavor)) return processPreCreateDamageRoll(data, options);
    if (data.flavor.includes(savingThrowText)) return processpreCreateSaveRoll(data, options);
    return true;
  });

  Hooks.on("renderChatMessage", (message, html, data) => {
    if (checkBetterRolls && message?.data?.content?.startsWith('<div class="dnd5e red-full chat-card"')) return processBetterRollsChatCard(message, html, data)
    if (debug) log("renderChatMessage hook", message, html, data, attackRollFlavor, damageRollFlavor);
      if (!message.data || !message.data.flavor) return true;
      let flavor = message.data.flavor;


      if (!flavor) return true;
      if (flavor && flavor.includes(i18n("minor-qol.saving-throw"))) return processMQSaveRoll(message, html, data);
      if (flavor && flavor.startsWith(undoDamageText)) return processUndoDamageCard(message, html, data);
      if (!message.isRoll) return true;
      if (flavor && flavor.includes(damageRollFlavor)) return processDamageRoll(message, html, data);
      if (flavor && flavor.includes(attackRollFlavor)) return processAttackRoll(message, html, data);
      if (flavor && flavor.includes(savingThrowText)) return processSaveRoll(message, html, data);
      return true;
  });
};


/**
 * 
 * @param {Item} item 
 * @param {integer} slot 
 * 
 * If no macro exists create a macro roll
 */
async function createMinorQOLMacro(item, slot) {
    const command = `MinorQOL.doRoll(event, "${item.name}", {type: "${item.type}", versatile: false});`;
    let macro = game.macros.entities.find(m => m.name.startsWith(item.name)  &&  m.data.command === command);
    if (!macro) {
        macro = await Macro.create({
            name: `${item.name} - ${item.type}`,
            type: "script",
            img: item.img,
            command: command,
            flags: { "dnd5e.itemMacro": true }
        }, { displaySheet: false });
    }
    game.user.assignHotbarMacro(macro, slot);
}

export function doMacroRoll(event, itemName, itemType = "weapon") {
  return doRoll(event, itemName, {type: itemType});
}

export async function doRoll(event, itemName, {type = "weapon", versatile=false, token = null}={type:"weapon", versatile: false, token: null}) {
    if (!macroSpeedRolls) return game.dnd5e.rollItemMacro(itemName);
    // Get the syntehtic actor if there is one
    if (token) {
      var actor = typeof token === "string" ? canvas.tokens.get(token).actor : token.actor;
    } else {
      var speaker = ChatMessage.getSpeaker();
      var token = canvas.tokens.get(speaker.token);
      var actor = token ? token.actor : game.actors.get(speaker.actor);
    }
    if (!actor) {
      ui.notifications.warn(`${game.i18n.localize("minor-qol.noSelection")}`)
      return;
    } 
    if (!event) {
      event = {altKey: false, shiftKey: true, metaKey: false, ctrlKey: false, originalEvent: null};
    } else if (!event.originalEvent) {
      // from macros we get a mouse event so create a false event with the right shift key behaviour
      event = {altKey: event.altKey, shiftKey: event.shiftKey, metaKey: event.metaKey, ctrlKey: event.ctrlKey, originalEvent: event};
    }
    
    const item = actor ? actor.items.find(i => i.name === itemName && (!type || i.type === type)) : null;
    if (!item)
        return ui.notifications.warn(`${i18n("minor-qol.noItemNamed")} ${itemName}`);
    return await doCombinedRoll({actor, item, event, versatile});
}

let enableSheetQOL = (app, html, data) => {
    // find out how to reinstate the original handler later.
    const defaultTag = ".item .item-image";
    //Add a check for item deletion
    if (itemDeleteCheck) {
        // remove current handler - this is a bit clunky since it results in a case with no delete handler
        $(html).find(".item-delete").off("click");
        $(html).find(".item-delete").click({ app: app, data: data }, itemDeleteHandler);
    }

    let rollTag = knownSheets[app.constructor.name] ? knownSheets[app.constructor.name] : defaultTag;

    if (itemRollButtons)
        addItemSheetButtons(app, html, data);

    if (speedItemRolls) {
        // Item Rolling do attack and damge at the same
        $(html).find(rollTag).off("click");
        $(html).find(rollTag).click({ app, data, html }, itemRollHandler);
    }
    return true;
};


function addItemSheetButtons(app, html, data, triggeringElement = "", buttonContainer = "") {
    // Setting default element selectors
    if (triggeringElement === "")
        triggeringElement = ".item .item-name";

        if (["BetterNPCActor5eSheet", "BetterNPCActor5eSheetDark"].includes(app.constructor.name)) {
      triggeringElement = ".item .npc-item-name"
      buttonContainer = ".item-properties"
    }
    if (buttonContainer === "")
        buttonContainer = ".item-properties";

    // adding an event for when the description is shown
    html.find(triggeringElement).click(event => {
        let li = $(event.currentTarget).parents(".item");
        if (!li.hasClass("expanded")) return; 
        let item = app.object.getOwnedItem(li.attr("data-item-id"));
        if (!item) return;
        let actor = app.object;
        let chatData = item.getChatData();
        let targetHTML = $(event.target.parentNode.parentNode);
        let buttonTarget = targetHTML.find(".item-buttons");
        if (buttonTarget.length > 0) return; // already added buttons
        let buttonsWereAdded = false;
        // Create the buttons
        let buttons = $(`<div class="item-buttons"></div>`);
        switch (item.data.type) {
            case "weapon":
            case "spell":
            case "feat":
                if (speedItemRolls)
                    buttons.append(`<span class="tag"><button data-action="basicRoll">${i18n("minor-qol.buttons.roll")}</button></span>`);
                if (item.hasAttack)
                    buttons.append(`<span class="tag"><button data-action="attack">${i18n("minor-qol.buttons.attack")}</button></span>`);
                if (item.hasDamage)
                    buttons.append(`<span class="tag"><button data-action="damage">${i18n("minor-qol.buttons.damage")}</button></span>`);
                if (item.isVersatile) 
                    buttons.append(`<span class="tag"><button data-action="versatileAttack">${i18n("minor-qol.buttons.versatileAttack")}</button></span>`);
                if (item.isVersatile) 
                  buttons.append(`<span class="tag"><button data-action="versatileDamage">${i18n("minor-qol.buttons.versatileDamage")}</button></span>`);
//                if (speedItemRolls && item.hasSave && item.hasDamage)
//                    buttons.append(`<span class="tag"><button data-action="damage">${i18n("minor-qol.buttons.saveAndDamage")}</button></span>`);
                buttonsWereAdded = true;
                break;
            case "consumable":
                if (chatData.hasCharges)
                    buttons.append(`<span class="tag"><button data-action="consume">${i18n("minor-qol.buttons.itemUse")} ${item.name}</button></span>`);
                buttonsWereAdded = true;
                break;
            case "tool":
                buttons.append(`<span class="tag"><button data-action="toolCheck" data-ability="${chatData.ability.value}">${i18n("minor-qol.buttons.itemUse")} ${item.name}</button></span>`);
                buttonsWereAdded = true;
                break;
        }
        if (buttonsWereAdded) {
            buttons.append(`<br><header style="margin-top:6px"></header>`);
            // adding the buttons to the sheet

            targetHTML.find(buttonContainer).prepend(buttons);
            buttons.find("button").click({ app: app, data: data, html: html }, async (ev) =>  {
              console.warn("roll handler active")
                ev.preventDefault();
                ev.stopPropagation();
                if (debug) log("roll handler ", ev.target.dataset.action)
                // If speed rolls are off
                switch (ev.target.dataset.action) {
                    case "attack":
                        resetStatus();
                        await item.rollAttack({ event: ev });
                        break;
                    case "versatileAttack":
                        resetStatus();
                        minorQolStateData.versatile = true;
                        await item.rollAttack({ event: ev });
                        break;
                    case "damage":
                        await item.rollDamage({ event: ev, versatile: false });
                        break;
                    case "versatileDamage":
                        await item.rollDamage({ event: ev, versatile: true });
                        break;
                    case "consume":
                        await item.roll({ event: ev });
                        break;
                    case "toolCheck":
                        await item.rollToolCheck({ event: ev });
                        break;
                    case "basicRoll":
                        if (item.type === "spell") {
                          minorQolStateData.hitTargets = game.user.targets;
                          await actor.useSpell(item, { configureDialog: !ev.shiftKey });
                        }
                        else
                            await item.roll();
                        break;
                }
            });
        }
    });
}

async function itemRollHandler(event) {
  // Allow shift/ctl/alt from the weapon img - unshifted works as before
  if (event.__proto__.preventDefault)
      event.preventDefault();
  //let actor = game.actors.get(event.data.data.actor._id);
  let actor;
  // If the app has a token then this is a token sheet and we want the actor inside the token
  if (event.data.app.token)
      actor = event.data.app.token.actor;
  else if (event.data.app.object)
      actor = event.data.app.object;
  // this should be defined
  else
      actor = game.actors.get(event.data.data.actor._id); // but just in case we can get the global Actor if we must
  let itemId = $(event.currentTarget).parents(".item").attr("data-item-id");
  let item = actor.getOwnedItem(itemId);
  if (debug) log("itemRollhandler", itemId, item, actor)
  if (speedItemRolls) {
    if (item.hasAttck || item.hasSave || item.hasDamage || autoItemEffects) {
        return await doCombinedRoll({actor, item, event});
    }
    if (item.type === "spell")  {
      return await actor.useSpell(item, { configureDialog: !event.shiftKey });
    }
    if (item.type === "tool") {
      setShiftOnly(event);
      return await item.rollToolCheck({ event });
    }
    if (item.type === "consumable") {
      setShiftOnly(event);
      return await item.roll({event});
      // return rollConsumable({item, event});
    }
  }
  return await item.roll({event});
}

// Fires on renderMeasuredTemplate.
// set game user targets
let selectTargets = (scene, data, options) => {
  let targeting = autoTarget;
  if (data.user !== game.user._id) {
      return true;
  }
  if (targeting === "none") {
    Hooks.callAll("minor-qol-targeted", game.user.targets);
    return true;
  } 
  if (data) {
    // release current targets
    game.user.targets.forEach(t => {
      t.setTarget(false, { releaseOthers: false });
    });
    game.user.targets.clear();
  }

  let speaker = minorQolStateData.speaker;
  let item = minorQolStateData.item;
  // if the item specifies a range of "self" don't target the caster.
  let selfTarget = !(item && item.data.data.range && item.data.data.range.units === "self")

  let wallsBlockTargeting = targeting === "wallsBlock";
  let templateDetails = canvas.templates.get(data._id);
  let tdx = templateDetails.data.x;
  let tdy = templateDetails.data.y;
  canvas.tokens.placeables.filter(t => {
    if (!t.actor) return false;
    // skip the caster
    if (!selfTarget && speaker.token === t.id) return false;
    // skip special tokens with a race of trigger
    if (t.actor.data.data.details.race === "trigger") return false;
    if (!templateDetails.shape.contains(t.x + t.w / 2 - tdx, t.y + t.h / 2 - tdy))
      return false;
    if (!wallsBlockTargeting)
      return true;
    // construct a ray and check for collision
    let r = new Ray({ x: t.x + t.w / 2, y: t.y + t.h / 2 }, templateDetails.data);
    return !canvas.walls.checkCollision(r);
  }).forEach(t => {
    t.setTarget(true, { user: game.user, releaseOthers: false });
    game.user.targets.add(t);
  });
  game.user.broadcastActivity({targets: game.user.targets.ids});

  // Assumes area affect do not have a to hit roll
  minorQolStateData.saves = new Set();
  minorQolStateData.hitTargets = game.user.targets;
  Hooks.callAll("minor-qol-targeted", game.user.targets);
  return true;
};

let doCombinedRoll = async ({actor, item, event, versatile=false}) => {
  // stage 1 - do spell casting requirements.
  if (debug) log("docombinedRoll", actor, item, event);

  // Initiate ability template placement workflow if selected

  resetStatus(versatile);
  minorQolStateData.item = item;
  // If this is a spell, 
  // 1. Make sure there are spell slots to cast and consume spell slot.
  // record the spell level so the damage roll will work.
  // 2. If it is a template roll, draw the template and use that to tirgger setting targets
  // 3. Cause finish roll to be called either directly or after the template placement
  

  let result;
  if (item.type === "spell") {
   const itemData = item.data.data;

   // Configure spellcasting data
   let lvl = itemData.level;
   const usesSlots = (lvl > 0) && CONFIG.DND5E.spellUpcastModes.includes(itemData.preparation.mode);
   const limitedUses = !!itemData.uses.per;
   let consume = `spell${lvl}`;
   let placeTemplate = false;

   // Configure spell slot consumption and measured template placement from the form
   if ( usesSlots) {
    const target = item.data.data.target;
    item.data.data.target = null; // disable place template in cast dialog
    try {
      var spellFormData = await game.dnd5e.applications.SpellCastDialog.create(actor, item);
    } finally {
      item.data.data.target = target;
    }

    const isPact = spellFormData.get('level') === 'pact';
    lvl = isPact ? actor.data.data.spells.pact.level : parseInt(spellFormData.get("level"));
    if (Boolean(spellFormData.get("consume"))) {
      consume = isPact ? 'pact' : `spell${lvl}`;
    } else {
      consume = false;
    }
    placeTemplate = Boolean(spellFormData.get("placeTemplate"));

     // Create a temporary owned item to approximate the spell at a higher level
     /* This seems to break with minor-qol - come back to this TODO
     if ( lvl !== item.data.data.level ) {
       item = item.constructor.createOwned(mergeObject(item.data, {"data.level": lvl}, {inplace: false}), actor);
     }
     */
   }

   // Update Actor data
   if ( usesSlots && consume && (lvl > 0) ) {
     if (actor.data.data.spells[consume].value <= 0) {
      ui.notifications.warn(`${game.i18n.localize("minor-qol.NoSlots")}`)
      return;
     }
     await actor.update({
       [`data.spells.${consume}.value`]: Math.max(parseInt(actor.data.data.spells[consume].value) - 1, 0)
     });
   }

   // Update Item data
   if ( limitedUses ) {
     const uses = parseInt(itemData.uses.value || 0);
     if ( uses <= 0 ) ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", {name: item.name}));
     await item.update({"data.uses.value": Math.max(parseInt(item.data.data.uses.value || 0) - 1, 0)})
   }
   minorQolStateData.spellLevel = lvl;
  }
  // anything with an area of effect that might affect creatures
  let canAffectTargets = (item.hasAttack || item.hasSave || item.hasDamage); // query active effects?
  if (item.hasAreaTarget && canAffectTargets) {
    
    const template = AbilityTemplate.fromItem(item);
    // drawing the template removes all selected tokens - so remmber which one to get the right synthectic actor later.
    let token = canvas.tokens.controlled[0];
    // When the target is placed finish the roll
    Hooks.once("minor-qol-targeted", finishRoll.bind(null, {actor, item, event, token}));
    template.drawPreview(event);
    // hide the character sheet if displayed
    if (actor.sheet.rendered || actor.sheet._state === Application.RENDER_STATES.RENDERING) actor.sheet.minimize();

    if (token?.actor && token?.actor?.sheet?.rendered) token.actor.sheet.minimize();
    // don't continue the roll until the template is placed
    return false;
  }

  let targetDetails = item.data.data.target;
  if (rangeTarget && targetDetails && targetDetails.units === "ft" && ["creature", "ally", "enemy"].includes(targetDetails.type)) {
    const speaker = ChatMessage.getSpeaker();
    const token = canvas.tokens.get(speaker.token);
    if (!token) {
      ui.notifications.warn(`${game.i18n.localize("minor-qol.noSelection")}`)
      return true;
    }
      // We have placed an area effect template and we need to check if we over selected
    let dispositions = targetDetails.type === "creature" ? [-1,0,1] : targetDetails.type === "ally" ? [1] : [-1];
    // release current targets
    game.user.targets.forEach(t => {
      t.setTarget(false, { releaseOthers: false });
    });
    game.user.targets.clear();
    // calculate pixels eqivalent distance - requires map to be in ft.
    let minDist = targetDetails.value * canvas.grid.size / canvas.scene.data.gridDistance;
    canvas.tokens.placeables
          .filter(target => target.actor.data.data.details.race !== "trigger"
                            && target.actor.id !== token.actor.id
                            && dispositions.includes(target.data.disposition) 
                            && Math.hypot(token.center.x - target.center.x, token.center.y - target.center.y) <= minDist)
          .forEach(token=> {
            token.setTarget(true, { user: game.user, releaseOthers: false });
            game.user.targets.add(token);
          });

    // Assumes area affect do not have a to hit roll
    minorQolStateData.saves = new Set();
    minorQolStateData.hitTargets = game.user.targets;
    game.user.broadcastActivity({targets: game.user.targets.ids});
  }

  if (debug) log("about to call finish roll")
  return finishRoll({ actor, item, event});
};

let hasChargesAvailable = (item) => {
  const usesRecharge = !!item.data.data.recharge?.value;
  const uses = item.data.data.uses;
  let usesCharges = !!uses?.per && (uses.max > 0);
  if (!usesRecharge && !usesCharges) return true;
  if (usesRecharge && item.data.data.recharge.charged) return true;
  if (item.data.data.uses.value > 0) return true;
  ui.notifications.warn(`${game.i18n.localize("minor-qol.noCharges")}`)
 return false;
}
let consumeCharge = async (item) => {
    // Determine whether to deduct uses of the item
    let itemData = item.data.data;
    const uses = itemData.uses || {};
    let usesCharges = !!uses.per && (uses.max > 0);
    const recharge = itemData.recharge || {};
    const usesRecharge = !!recharge.value;
    if (!usesRecharge && !usesCharges) return true;
    // If no usages remain, display a warning
    const current = uses.value || 0;
    if ( current <= 0 ) ui.notifications.warn(game.i18n.format("DND5E.ItemNoUses", {name: item.name}));

    // Display a configuration dialog to confirm the usage
    let consume = uses.autoUse || true;

    // Update Item data
    if ( consume ) {
      const remaining = usesCharges ? Math.max(current - 1, 0) : current;
      if ( usesRecharge ) await item.update({"data.recharge.charged": false});
      else {
        await item.update({"data.uses.value": remaining});
      }
    }
}
/**
 * 
 * @param {Item, Event} Item that is rolling, event that generated the roll.
 * @param {*} targets optional list of targets to roll against
 */
let finishRoll = async ({ actor, item, event, token=null }, targets = null) => {
  // Spell has been cast if it is one.
  if (debug) log("finishRoll", item, event, token);
  let result;
  // Next do the attack Roll

  // if a token is passed we need to take control of it - else we will get the wrong speaker sent to the damage card
  if (token) token.control({ releaseOthers: true });

  // un target any deleted targets

  if (!event.originalEvent) {
    // from macros we get a mouse event so create a false event with the right shift key behaviour
    event = {
        altKey: event.altKey,
        shiftKey: event.shiftKey,
        metaKey: event.metaKey,
        ctrlKey: event.ctrlKey,
        originalEvent: event
    };
  }
  if (!(event.altKey || event.shiftKey || event.metaKey || event.ctrlKey)) {
      setShiftOnly(event);
  }
  if (item.data.type === "consumable") {
    // consumables need to be checked for charges before doing the rest of the effects.
    if (!hasChargesAvailable(item)) return false;
    item._rollConsumable(false);
  }

  if (item.hasAttack) {
      if (!(event.altKey || event.shiftkey || event.ctrlKey || event.metaKey)) setShiftOnly(event);
    // rollAttack consumes resources but not charges
    return item.rollAttack({ event, isCard: true});
    // return true;
  }

  if (item.hasDamage && autoRollDamage !== "none") {
    // A Damage roll without an attck roll - need to account for resource consumption ourselves
    const allowed = await item._handleResourceConsumption({isCard: true, isAttack: true});
    if (!allowed) return false;
    return item.rollDamage({ 
      event, 
      spellLevel: minorQolStateData.spellLevel, 
      versatile: minorQolStateData.versatile
    });
  }

  if(item.hasSave) { // no damage/attack but still a save is required
    const allowed = await item._handleResourceConsumption({isCard: true, isAttack: false});
    if (!allowed) return false;
    if (autoCheckSaves !== "none") {
      await checkSaves(game.user.targets, item, ["gmroll", "blindroll"].includes(game.settings.get("core", "rollMode")));
      if (autoItemEffects && dynamicEffectsActive) 
        DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: minorQolStateData.failedSaves, 
                                  whisper: true, spellLevel: minorQolStateData.spellLevel});
      resetStatus();                                  
      return true;
    }
  }

  if (!hasChargesAvailable(item)) return false;
  if (item.type === "tool") {
    result = await item.rollToolCheck({event});
  } else {
    result = await item.roll({configureDialog: false});
  }

  // if we get here then thre is no attack/damage/save for the item. So the only thing that might happen is applying item effects
  if (autoItemEffects && dynamicEffectsActive) {
    DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: game.user.targets, 
      whisper: true, spellLevel: minorQolStateData.spellLevel}) 
      resetStatus();
  }
  return result;
};

let filterTargets = (removeAll = false) => {
  setTimeout(() => {
    if (autoRemoveTargets === "none") return;
    let newTargets = new Set();
    game.user.targets.forEach(t=> {
      if (t.actor.data.data.attributes.hp.value <= 0 || removeAll) {
        t.setTarget(false, { user: game.user, releaseOthers: false });
      } else {
        newTargets.add(t);
      }
    })
    game.user.targets = newTargets;
    minorQolStateData.hitTargets = newTargets;
    game.user.broadcastActivity({targets: game.user.targets.ids});
  }, (autoApplyDamage && autoRemoveTargets === "dead") ? 250 : 0); 
  // if removing dead and applying damage wait a bit for the gm client to do it's thing
}

let removeAllTargets = () => {
  game.user.targets.forEach(t=> {
      t.setTarget(false, { user: game.user, releaseOthers: false });
  })
  game.user.targets = new Set();
}

Hooks.on("renderChatMessage", (message, html) => {
  if (coloredBorders === "none") return true;
  html[0].style.borderColor = game.users.get(message.data.user).color;
  if (coloredBorders === "borderNames") html[0].children[0].children[0].style.backgroundColor = game.users.get(message.data.user).color;
  return true;
})

let processPreCreateAttackRoll =  async (data, options) => {
  let theTargets = game.user.targets;
  let isCritical, isFumble;
  let isHit = true;
  let attackRoll, attackTotal, actor, item;
  if (debug) log("preprocessAttackRoll |", speedItemRolls, autoCheckHit, autoApplyDamage)
  if (speedItemRolls || autoCheckHit !== "none") {
    attackRoll = Roll.fromData(JSON.parse(data.roll));
    attackTotal = attackRoll.total;
    actor = game.actors.tokens[data.speaker.token];
    if (!actor) actor = game.actors.get(data.speaker.actor);
    item = actor.items.find(i => i.hasAttack && data.flavor.startsWith(`${i.name}${attackRollFlavor}`));
  }

  // Assume we have a single die result
  isCritical = attackRoll.parts[0].results[0] >= attackRoll.parts[0].options.critical;
  isFumble = attackRoll.parts[0].results[0] <= attackRoll.parts[0].options.fumble;
        
  if (autoCheckHit !== "none") {
    // let theTargets = game.user.targets;
    let msg = "";
    let sep = "";
    
    // check for a hit/critical/fumble
    isHit = false;
    theTargets = new Set();
    let hitDisplay = [];

    for (let targetToken of game.user.targets) {
      let targetActor = targetToken.actor;
      if (!isFumble && !isCritical) {
          // check to see if the roll hit the target
          let targetAC = targetActor.data.data.attributes.ac.value;
          if (game.user.isGM) log(`${data.speaker.alias} Rolled a ${attackTotal} to hit ${targetActor.name}s AC of ${targetAC}`);
          isHit = attackTotal >= targetAC;
      }
      // Log the hit on the target
      let attackType = item?.name ? i18n(item.name) : "Attack";
      let hitString = isCritical ? i18n("minor-qol.criticals") : isFumble? i18n("minor-qol.fumbles") : isHit ? i18n("minor-qol.hits") : i18n("minor-qol.misses");
      hitDisplay.push({isPC: targetToken.actor.isPC, target: targetToken, hitString, attackType});

      // If we hit and we have targets and we are applying damage say so.
      if (isHit || isCritical) theTargets.add(targetToken);
    }
    let templateData = {
      hits: hitDisplay, 
      isGM: game.user.isGM,
      damageAppliedString: autoApplyDamage !== "none" && theTargets.size > 0 && autoRollDamage !== "none" ? i18n("minor-qol.damage-applied") : ""
    }
    let content = await renderTemplate("modules/minor-qol/templates/hits.html", templateData);
    if (game.user.targets.size > 0) {
      let chatData = {
        user: game.user._id,
        speaker: { actor: actor, alias: actor.name },
        content: content,
        type: CONST.CHAT_MESSAGE_TYPES.OTHER,
        flags: { minorQolType: MESSAGETYPES.hitData }
      }
      if (autoCheckHit === "whisper" || data.whisper) 
      {
        chatData.whisper = ChatMessage.getWhisperRecipients("GM");
        chatData.user = ChatMessage.getWhisperRecipients("GM")[0];
      }
      ChatMessage.create(chatData);
    }
  }
  minorQolStateData.hitTargets = theTargets;
  let shouldRollDamage = item.hasDamage && autoRollDamage !== "none" && !isFumble && (
                      ((game.user.targets.size === 0) || // nothing selected so roll damage if auto enabled
                      (autoRollDamage === "always") ||
                      (autoCheckHit === "none") || // we are not checking for hits so roll damage
                      (theTargets.size > 0))); // we actually hit something
  if (debug) log("process attack roll - roll damage?", shouldRollDamage, item, item.hasDamage, autoRollDamage, game.user.targets.size, autoCheckHit, theTargets.size);
  let event = {};
  if (isCritical)
      setAltOnly(event);
  else
      setShiftOnly(event);
  
  if (shouldRollDamage) {
    // Chain the damage roll if required
    // wait for the chat message to appear before going on to roll damage
    Hooks.once("renderChatMessage", async () => { 
      await item.rollDamage({ 
        event, 
        spellLevel: minorQolStateData.spellLevel, 
        versatile: minorQolStateData.versatile
      });
    })
  } else if (autoItemEffects && dynamicEffectsActive && theTargets && theTargets.size > 0 && autoApplyDamage !== "none") { // no need to roll damage but perhaps apply item effects
    // assume effects only applied to hit targets
    DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: theTargets, whisper: true, spellLevel: minorQolStateData.spellLevel});
    resetStatus();
  }
  return true;
};

/**
 * If we roll an attack and we are not auto rolling damage add a roll damage button to the attack roll
 * If addDamageButtons add damage buttons to the roll
 * 
 * @param {ChatMessage} message 
 * @param {*} html 
 * @param {*  } data 
 */
let processAttackRoll = async (message, html, data) => {
  if (debug) log("processAttack", message, message.data.user);
  if (autoRollDamage !== "none" || !speedItemRolls) return true;

  let actor = game.actors.tokens[message.data.speaker.token];
  if (!actor) actor = game.actors.get(message.data.speaker.actor);
  // this is called after preCreate
  let item = actor.items.find(i => message.data.flavor.startsWith(`${i.name}${attackRollFlavor}`) && i.hasDamage);
  //if (!item) item = minorQolStateData.item;
  if (!item) return;
  html.find(".message-content").append('<button class="minor-qol-roll-damage-button dnd5e chat-card card-buttons button" data-action=""> Roll Damage')
  let button = html.find(".minor-qol-roll-damage-button");
  button.off("click");
  button.click(async (ev) => {
        ev.stopPropagation();
        await item.rollDamage({event: ev})
  });
  return true;

  // <button data-action="damage">
}

// process a saving throw to see if it 
let processSaveRoll = (message, html, data) => {
  const userId = message.user.id;
  if (!saveRequests[userId]) return true;
  const total = message._roll._total;
  const formula = message._roll._formula;
  saveRequests[userId]({total, formula})
  delete saveRequests[userId];
  return true;
}

let processMQSaveRoll = async (message, html, data) => {
  if (debug) log("processSave", message, message.data.user);
  if (autoRollDamage !== "none") return true;
  let actor = game.actors.tokens[message.data.speaker.token];
  if (!actor) actor = game.actors.get(message.data.speaker.actor);
  // this is called after preCreate
  const itemRe = new RegExp(`\\s*(.*)\\s+DC[\\s0-9]*.*${i18n("minor-qol.saving-throw")}`)
  const itemName = message.data.flavor.match(itemRe);
  if (!itemName) {
    console.warn("Minor-qol | could not find item name", itemName, itemRe)
    return;
  }
  const item = actor.items.find(i => i.name === itemName[1] && i.hasDamage);
  //if (!item) item = minorQolStateData.item;
  if (!item?.hasDamage) return;
  html.find(".message-content").append('<button class="minor-qol-roll-damage-button dnd5e chat-card card-buttons button" data-action=""> Roll Damage')
  let button = html.find(".minor-qol-roll-damage-button");
  button.off("click");
  button.click(async (ev) => {
        ev.stopPropagation();
        const saveAutoCheckSaves = autoCheckSaves;
        autoCheckSaves = "none";
        await item.rollDamage({event: ev})
        autoCheckSaves = saveAutoCheckSaves;
  });
  return true;

  // <button data-action="damage">
}
/**
 * If autoApplyDamge is set apply the damage to minor-qol.hitTargets using immunities and saving throws if appropriate.
 * If addDamageButtons add damage buttons to the roll
 * 
 * @param {ChatMessage} message 
 * @param {*} html 
 * @param {*  } data 
 */
let processDamageRoll = async (message, html, data) => {
  if (debug) log("processDamageRoll", message, message.data.user);
  if (message.user.id !== game.user.id) return true;
  // proceed if adding chat damage buttons or applying damage for our selves
  if (!addChatDamageButtons && (autoApplyDamage === "none" || message.data.user !==  game.user._id)) {
    resetStatus();
   return true;
  }

  let actor = game.actors.tokens[message.data.speaker.token];
  if (!actor) actor = game.actors.get(message.data.speaker.actor);
  // this is called after preCreate
  let item = actor?.items.find(i => message.data.flavor.startsWith(`${i.name}${damageRollFlavor}`) && i.hasDamage);
  if (!item) item = minorQolStateData.item;
  // if (!item) return;
  let totalDamage = message.roll.total;
  let damageDetail = createDamageList(message.roll, item);
  let theTargets = minorQolStateData.hitTargets;
  checkSaves(game.user.targets, item, data.whisper !== undefined).then((saves) => {
    if (autoApplyDamage !== "none" && game.user._id === message.data.user) {
      applyTokenDamage(damageDetail, totalDamage, theTargets, item, saves);
    }
    if (item && autoItemEffects && !message.data.flags?.noDynamicEffects && dynamicEffectsActive && message.data.user ===  game.user._id && theTargets && theTargets.size > 0) { // perhaps apply item effects
      // if someone saved we want the failedSaves to apply effects if no-one saved then all targets get the effect.
      if (saves.size > 0) theTargets = minorQolStateData.failedSaves;
      // assume effects only applied to hit targets
      if (autoCheckHit !== "none" || (autoCheckSaves !== "none" && theTargets.size > 0)) {
        if (debug) log("processDamage Roll - about to call doEffects ", item, item.actor, theTargets)
        let spellLevel = minorQolStateData.spellLevel;
        DynamicEffects.doEffects({item, actor: item.actor, activate: true, targets: theTargets, 
                                whisper: true, spellLevel}) 
      }
    }
    resetStatus();
    filterTargets();   
  });
  if (addChatDamageButtons) {
    addDamageButtons(damageDetail, totalDamage, html);
  }
  return true;
};

let processUndoDamageCard = async(message, html, data) => {
  message.data.flags["minor-qol"] && message.data.flags["minor-qol"].forEach(({tokenID, oldTempHP, oldHP}) => {
    let token = canvas.tokens.get(tokenID);
    if (!token) {
      log(`Token ${tokenID} not found`);
      return;
    }
    let button = html.find(`#${tokenID}`);
    button.click(async (ev) => {
      log(`Setting HP back to ${oldTempHP} and ${oldHP}`);
      let actor = canvas.tokens.get(tokenID).actor;
      await actor.update({ "data.attributes.hp.temp": oldTempHP, "data.attributes.hp.value": oldHP });
      ev.stopPropagation();
    });
  })
}
/**
 * 
 * @param {Set} theTargets 
 * @param {Item} item 
 * 
 * Return a Set of successful saves from the set of tokens theTargets.
 */
let checkSaves = async (theTargets, item, whisper = false) => {
    let saves = new Set();
    if (autoCheckSaves === "none") return saves;
    let failedSaves = new Set()
    let saveDisplay = [];
    if (item.hasSave && theTargets.size > 0) {
      // requestPCSave(item.data.data.save.ability);
        theTargets = game.user.targets;
        minorQolStateData.hitTargets = game.user.targets;
        let rollDC = item.data.data.save.dc;
        let rollAbility = item.data.data.save.ability;
        let message = "";
        let promises = [];
        ;
        minorQolStateData.saveCount = theTargets.size;
        // make sure saving throws are renabled.
        try {
          saveRequests = {}
          minorQolStateData.hideSaves = true;
          for (let target of theTargets) {
            let event = {shiftKey: true};
            let advantage = (target?.actor?.data?.data?.traits?.dr?.custom || "").includes(i18n("minor-qol.MagicResistant")) && item.data.type === "spell";
            const player = game.users.players.find(p=> p.character?._id === target.actor._id)
            event.altKey = advantage;
            if (playerRollSaves !== "none" && player?.active) {
              minorQolStateData.saveCount -= 1;
              console.log(`minor-qol - Player ${player.name} controls actor ${target.actor.name} - requesting ${CONFIG.DND5E.abilities[item.data.data.save.ability]} save`);
              promises.push(new Promise((resolve, reject) => {
                const requestId = player.id;
                saveRequests[requestId] = resolve;
                requestPCSave(item.data.data.save.ability, player.id, target.actor.id, advantage)
                // set a timeout for taking over the roll
                setTimeout(() => {
                  console.warn(`minor-qol | Timeout waiting for ${player.name} to roll ${CONFIG.DND5E.abilities[item.data.data.save.ability]} save - rolling for them`)
                  if (saveRequests[player.id]) {
                      let result = target.actor.rollAbilitySave(item.data.data.save.ability, {event, advantage});
                      delete saveRequests[player.id];
                      resolve(result)
                  }
                }, playerSaveTimeout * 1000);
              }))
            } else {
              promises.push(target.actor.rollAbilitySave(item.data.data.save.ability, {event, advantage}));
            }
          }
        } catch (err) {
            console.warn(err)
        } finally {
          minorQolStateData.hideSaves = minorQolStateData.saveCount > 0;
        }

        let results = await Promise.all(promises);
        saveRequests = {};
        if (debug) log("Results are ", results);
        let i = 0;
        for (let target of theTargets) {
            let rollTotal = results[i].total;
            let saved = rollTotal >= rollDC;
            if (saved)
              saves.add(target);
            else
              failedSaves.add(target);
            if (game.user.isGM) log(`Ability save result is ${target.name} rolled ${rollTotal} vs ${rollAbility} DC ${rollDC}`);
            let saveString = i18n(saved ? "minor-qol.save-success" : "minor-qol.save-failure");
            let noDamage = saved && getSaveMultiplierForSpell(item) === 0 ? i18n("minor-qol.noDamage") : "";
            let adv = results[i].formula.includes("kh") ? `(${i18n("minor-qol.advantage")})` : "";
            saveDisplay.push({name: target.name, img: target.data.img, isPC: target.actor.isPC, target, saveString, rollTotal, noDamage, id: target.id, adv});
            i++;
        }
        let templateData = {
          saves: saveDisplay, 
          damageAppliedString: autoRollDamage !== "none" && autoApplyDamage !== "none" && item.hasDamage ? i18n("minor-qol.damage-applied") : ""
         }
        let content = await renderTemplate("modules/minor-qol/templates/saves.html", templateData);
        let chatData = {
          user: game.user._id,
          speaker: { actor: item.actor, alias: item.actor.name },
          content: `<div data-item-id="${item._id}"></div> ${content}`,
          flavor: `<h4"> ${item.name} DC ${rollDC} ${CONFIG.DND5E.abilities[rollAbility]} ${i18n(theTargets.size > 1 ? "minor-qol.saving-throws" : "minor-qol.saving-throw")}:</h4>`,
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
          flags: { minorQolType: MESSAGETYPES.saveData }
        };
        if (autoCheckSaves === "whisper" || whisper) {
          chatData.whisper = ChatMessage.getWhisperRecipients("GM");
          chatData.user = ChatMessage.getWhisperRecipients("GM")[0];
        }
        minorQolStateData.saves = saves;
        minorQolStateData.failedSaves = failedSaves;
        await ChatMessage.create(chatData);
    }
    minorQolStateData.saves = saves;
    minorQolStateData.failedSaves = failedSaves;
    return saves;
};

// Calculate the hp/tempHP lost for an amount of damage of type
function calculateDamage(a, appliedDamage, t, totalDamage, dmgType) {
  let value = Math.floor(appliedDamage);
  if (dmgType === "temphp") { // only relavent for healing of tmp HP
    var hp = a.data.data.attributes.hp;
    var tmp = parseInt(hp.temp) || 0;
    var oldHP = hp.value;
    var newTemp = Math.clamped(tmp - value, 0, hp.tempmax);
    var newHP = hp.value;
  } else {
    var hp = a.data.data.attributes.hp, tmp = parseInt(hp.temp) || 0, dt = value > 0 ? Math.min(tmp, value) : 0;
    var newTemp = tmp - dt;
    var oldHP = hp.value;
    var newHP = Math.clamped(hp.value - (value - dt), 0, hp.max);
  }

  if (game.user.isGM)
      log(`${a.name} takes ${value} reduced from ${totalDamage} Temp HP ${newTemp} HP ${newHP}`);
  return {tokenID: t.id, actorID: a._id, tempDamage: dt, hpDamage: oldHP - newHP, oldTempHP: tmp, newTempHP: newTemp,
          oldHP: oldHP, newHP: newHP, totalDamage: totalDamage, appliedDamage: value};
}


/** 
 * Work out the appropriate multiplier for DamageTypeString on actor
 * If DamageImmunities are not being checked always return 1
 * 
 */

let getTraitMult = (actor, dmgTypeString) => {
  if (damageImmunities) {
    if (dmgTypeString !== "") {
      if (actor.data.data.traits.di.value.includes(dmgTypeString)) return 0;
      if (actor.data.data.traits.dr.value.includes(dmgTypeString)) return 0.5;
      if (actor.data.data.traits.dv.value.includes(dmgTypeString)) return 2;
    }
    if (dmgTypeString === "healing" || dmgTypeString === "temphp") return -1;
    // Check the custom immunities
  }
  return 1;
};

let _highlighted = null;

let _onTargetHover = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
//  const li = event.currentTarget;
//  const token = canvas.tokens.get(li.id);
  const token = canvas.tokens.get(event.currentTarget.id);
  if ( token?.isVisible ) {
    if ( !token._controlled ) token._onHoverIn(event);
    _highlighted = token;
  }
}

/* -------------------------------------------- */

/**
 * Handle mouse-unhover events for a combatant in the tracker
 * @private
 */
let _onTargetHoverOut = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
  if (_highlighted ) _highlighted._onHoverOut(event);
  _highlighted = null;
}

let _onTargetSelect = (event) => {
  event.preventDefault();
  if ( !canvas.scene.data.active ) return;
  const token = canvas.tokens.get(event.currentTarget.id);
  token.control({ multiSelect: false, releaseOthers: true });
};


/**
 * If this is a minor qol roll (save or hits) make the actor name buttons select the token for the GM.
 */
let setupHiddenNameButtons = () => {
  Hooks.on("renderChatMessage", (message, html, data) => {
    if (![MESSAGETYPES.hitData, MESSAGETYPES.saveData].includes(getProperty(message.data.flags, "minorQolType")))
      return;
    let ids = html.find(".minor-qol-target-name")
    // let buttonTargets = html.getElementsByClassName("minor-qol-target-npc");

    ids.hover(_onTargetHover, _onTargetHoverOut)

    if (game.user.isGM)  {
      ids.click(_onTargetSelect);
    }
    if (!game.user.isGM && hideNPCNames) {
      ids=html.find(".minor-qol-target-npc");
      ids.text("-???-");
    }
  });
}

/**
 *  return a list of {damage: number, type: string} for the roll and the item
 */
let createDamageList = (roll, item) => {
  let damageList = [];

  let rollParts = roll.parts;
  let partPos = 0;
  let evalString;
  let damageSpec = item ? item.data.data.damage : {parts: []};
  if (debug) log("Passed roll is ", roll)
  if (debug) log("Damage spec is ", damageSpec)
  for (let [spec, type] of damageSpec.parts) {
    if (debug) log("single Spec is ", spec, type, item)
    if (item) {
      var rollSpec = new Roll(spec, item.actor?.getRollData() || {}).roll();
    }
    if (debug) log("rollSpec is ", spec, rollSpec)
    let specLength = rollSpec.parts.length;
    evalString = "";

    if (debug) log(specLength, rollSpec.parts)
    for (let i = 0; i < specLength && partPos < rollParts.length; i++) {
      if (typeof rollParts[partPos] !== "string") {
        if (debug) log("roll parts ", rollParts[partPos])
        let total = rollParts[partPos].total;
        evalString += total;
      }
      else evalString += rollParts[partPos];
      partPos += 1;
    }
    let damage = new Roll(evalString).roll().total;
    if (debug) log("Damage is ", damage, type, evalString)
    damageList.push({ damage: damage, type: type });
    partPos += 1; // skip the plus
  }
  if (debug) log(partPos, damageList)
  evalString = "";
  while (partPos < rollParts.length) {
    if (debug) log(rollParts[partPos])
    if (typeof rollParts[partPos] !== "string") {
      let total = rollParts[partPos].total;
      evalString += total;
    }
    else evalString += rollParts[partPos];
    partPos += 1;
  }
  if (evalString.length > 0) {
    if (debug) log("Extras part is ", evalString)
      let damage = new Roll(evalString).roll().total;
      let type = damageSpec.parts[0] ? damageSpec.parts[0][1] : "radiant";
      damageList.push({ damage, type});
      if (debug) log("Extras part is ", evalString)

  }
  if (debug) log("Final damage list is ", damageList)
  return damageList;
};
let applyTokenDamage = (damageDetail, totalDamage, theTargets, item, saves) => {
    let damageList = [];
    if (item && item.data.data.target?.type === "self") {
      theTargets = new Set();
      theTargets.add(canvas.tokens.get(ChatMessage.getSpeaker().token));
    }
    if (debug) log("applyTokenDame - targets", theTargets)
    if (!theTargets || theTargets.size === 0) {
      // probably called from refresh - don't do anything
      resetStatus();
      return true;
    }
    for (let t of theTargets) {
        let a = t.actor;
        let appliedDamage = 0;
        for (let { damage, type } of damageDetail) {
            let mult = saves.has(t) ? getSaveMultiplierForSpell(item) : 1;
            mult = mult * getTraitMult(a, type);
            appliedDamage += Math.floor(damage * Math.abs(mult)) * Math.sign(mult);
            var dmgType = type;
        }
        damageList.push(calculateDamage(a, appliedDamage, t, totalDamage, dmgType));
    }
    if (theTargets.size > 0) {
      let intendedGM = game.users.entities.find(u => u.isGM && u.active);
      if (!intendedGM) {
        ui.notifications.error(`${game.user.name} ${i18n("minor-qol.noGM")}`);
        console.error("Minor Qol | No GM user connected - cannot apply damage");
      }
      broadcastData({
            action: "reverseDamageCard",
            sender: game.user.name,
            intendedFor: intendedGM,
            damageList: damageList
        });
    }
};

let addDamageButtons = async (damageDetail, totalDamage, html) => {
    const btnContainer = $('<span class="dmgBtn-container" style="position:absolute; right:0; bottom:1px;"></span>');
    let btnStyling = "width: 22px; height:22px; font-size:10px;line-height:1px";
    const fullDamageButton = $(`<button class="dice-total-full-damage-button" style="${btnStyling}"><i class="fas fa-user-minus" title="Click to apply full damage to selected token(s)."></i></button>`);
    const halfDamageButton = $(`<button class="dice-total-half-damage-button" style="${btnStyling}"><i class="fas fa-user-shield" title="Click to apply half damage to selected token(s)."></i></button>`);
    const doubleDamageButton = $(`<button class="dice-total-double-damage-button" style="${btnStyling}"><i class="fas fa-user-injured" title="Click to apply double damage to selected token(s)."></i></button>`);
    const fullHealingButton = $(`<button class="dice-total-full-healing-button" style="${btnStyling}"><i class="fas fa-user-plus" title="Click to apply full healing to selected token(s)."></i></button>`);
    btnContainer.append(fullDamageButton);
    btnContainer.append(halfDamageButton);
    btnContainer.append(doubleDamageButton);
    btnContainer.append(fullHealingButton);
    html.find(".dice-total").append(btnContainer);
    // Handle button clicks
    let setButtonClick = (buttonID, mult) => {
        let button = html.find(buttonID);
        button.off("click");
        button.click(async (ev) => {
            ev.stopPropagation();
            if (canvas.tokens.controlled.length === 0) {
                console.warn(`Minor-qol | user ${game.user.name} ${i18n("minor-qol.noTokens")}`);
                return ui.notifications.warn(`${game.user.name} ${i18n("minor-qol.noTokens")}`);
            }
            // find solution for non-magic weapons
            let promises = [];
            for (let t of canvas.tokens.controlled) {
                let a = t.actor;
                let appliedDamage = 0;
                for (let { damage, type } of damageDetail) {
                    let typeMult = mult * Math.abs(getTraitMult(a, type)); // ignore damage type for buttons
                    appliedDamage += Math.floor(Math.abs(damage * typeMult)) * Math.sign(typeMult);
                }
                let damageItem = calculateDamage(a, appliedDamage, t, totalDamage, "");
                promises.push(a.update({ "data.attributes.hp.temp": damageItem.newTempHP, "data.attributes.hp.value": damageItem.newHP }));
            }
            let retval = await Promise.all(promises);
            return retval;
        });
    };
    setButtonClick(".dice-total-full-damage-button", 1);
    setButtonClick(".dice-total-half-damage-button", 0.5);
    setButtonClick(".dice-total-double-damage-button", 2);
    setButtonClick(".dice-total-full-healing-button", -1);
};

let itemDeleteHandler = ev => {
  let actor = game.actors.get(ev.data.data.actor._id);
  let d = new Dialog({
      // localize this text
      title: i18n("minor-qol.reallyDelete"),
      content: `<p>${i18n("minor-qol.sure")}</p>`,
      buttons: {
          one: {
              icon: '<i class="fas fa-check"></i>',
              label: "Delete",
              callback: () => {
                  let li = $(ev.currentTarget).parents(".item"), itemId = li.attr("data-item-id");
                  ev.data.app.object.deleteOwnedItem(itemId);
                  li.slideUp(200, () => ev.data.app.render(false));
              }
          },
          two: {
              icon: '<i class="fas fa-times"></i>',
              label: "Cancel",
              callback: () => { }
          }
      },
      default: "two"
  });
  d.render(true);
};

let setShiftOnly = event => {
    event.shiftKey = true;
    event.altKey = false;
    event.ctrlKey = false;
    event.metaKey = false;
};
let setAltOnly = event => {
    event.shiftKey = false;
    event.altKey = true;
    event.ctrlKey = false;
    event.metaKey = false;
};

let moduleSocket = "module.minor-qol";

let createReverseDamageCard = async (data) => {
    let whisperText = "";
    const damageList = data.damageList;
    const btnStyling = "width: 22px; height:22px; font-size:10px;line-height:1px";
    let token, actor;
    const timestamp = Date.now();
    let sep = "";
    let promises = [];
    let tokenIdList = [];
    for (let { tokenID, actorID, tempDamage, hpDamage, mult, oldTempHP, newTempHP, oldHP, newHP, totalDamage, appliedDamage } of damageList) {
        token = canvas.tokens.get(tokenID);
        actor = token.actor;
        tokenIdList.push({tokenID, oldTempHP, oldHP});
        if (data.intendedFor._id === game.user._id) {
            promises.push(actor.update({ "data.attributes.hp.temp": newTempHP, "data.attributes.hp.value": newHP }));
        }
        let buttonID = `${token.id}`;
        let btntxt = `<button id="${buttonID}"style="${btnStyling}"><i class="fas fa-user-plus" title="Click to reverse damage."></i></button>`;
        let tokenName = token.name ? `<strong>${token.name}</strong>` : token.actor.name;
        let dmgSign = appliedDamage < 0 ? "+" : "-"; // negative damage is added to hit points
        if (oldTempHP > 0)
            whisperText = whisperText.concat(`${sep}${duplicate(btntxt)} ${tokenName}<br> (${oldHP}:${oldTempHP}) ${dmgSign} ${Math.abs(appliedDamage)}[${totalDamage}] -> (${newHP}:${newTempHP})`);
        else
            whisperText = whisperText.concat(`${sep}${duplicate(btntxt)} ${tokenName}<br> ${oldHP} ${dmgSign} ${Math.abs(appliedDamage)}[${totalDamage}] -> ${newHP}`);
        ["di", "dv", "dr"].forEach(trait => {
          if (actor.data.data.traits[trait].custom) {
            whisperText = whisperText.concat(`<br>${trait}: ${actor.data.data.traits[trait].custom}`);
          }
        });
        sep = "<br>";
    }

    if (autoApplyDamage === "yesCard") {
      let message = await ChatMessage.create({
          user: game.user._id,
          speaker: { actor: actor},
          content: whisperText,
          whisper: ChatMessage.getWhisperRecipients("GM"),
          flavor: `${i18n("minor-qol.undoDamageFrom")} ${data.sender}`,
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
          flags: {"minor-qol": tokenIdList}
      });
    }
    await Promise.all(promises);
};

let processAction = data => {
  console.warn("Process Action data is:", data)
    switch (data.action) {
        case "reverseDamageCard":
            if (!game.user.isGM)
                break;
            if (autoApplyDamage == "none")
                break;
            createReverseDamageCard(data);
            break;
    }
};
let setupSocket = () => {
    game.socket.on(moduleSocket, data => {
        processAction(data);
    });
};

function broadcastData(data) {
    // if not a gm broadcast the message to a gm who can apply the damage
    if (!game.user.isGM)
      game.socket.emit(moduleSocket, data, resp => { });
    else
      processAction(data);
}

var dynamicEffectsActive;
var aboutTimeActive;
var betterRollsActive;
var lmrtfyActive;

Hooks.once("ready", () => {
    setupSocket();
    noDamageSaves = i18n("minor-qol.noDamageonSaveSpells");
    dynamicEffectsActive = game.modules.get("dynamiceffects") && game.modules.get("dynamiceffects").active;
    aboutTimeActive = game.modules.get("about-time") && game.modules.get("about-time").active;
    checkBetterRolls = false;
    betterRollsActive = game.modules.get("betterrolls5e") && game.modules.get("betterrolls5e").active;
    if (betterRollsActive) {
      checkBetterRolls = true;
      checkBetterRolls = game.settings.get("betterrolls5e", "d20Mode") === 1;
    }
    lmrtfyActive = game.modules.get("lmrtfy") && game.modules.get("lmrtfy").active;
});

let getSaveMultiplierForSpell = item => {
  // find a better way for this ? perhaps item property
  return noDamageSaves.includes(item.name) ? 0 : 0.5;
};

let i18n = key => {
  return game.i18n.localize(key);
};

knownSheets = {
  BetterNPCActor5eSheet: ".item .rollable",
  ActorSheet5eCharacter: ".item .item-image",
  BetterNPCActor5eSheetDark: ".item .rollable",
  ActorSheet5eCharacterDark: ".item .item-image",
  DarkSheet: ".item .item-image",
  DynamicActorSheet5e: ".item .item-image",
  ActorSheet5eNPC: ".item .item-image",
  DNDBeyondCharacterSheet5e: ".item .item-name .item-image",
  Tidy5eSheet:  ".item .item-image"

//  Sky5eSheet: ".item .item-image",
};

// local version of deselection that respoects GM requirement

class Deselection {
	static patchFunction(func, line_number, line, new_line) {
		let funcStr = func.toString()
		let lines = funcStr.split("\n")
		if (lines[line_number].trim() == line.trim()) {
			let fixed = funcStr.replace(line, new_line)
			return Function('"use strict";return (function ' + fixed + ')')();
		}
		return func;
	}
	static init() {
		Canvas.prototype._onMouseDown = Deselection.patchFunction(
			Canvas.prototype._onMouseDown,
			23,
			"event.data._selectState = 1;",
			`if (game.user.isGM && canvas.ready && Object.keys(canvas.activeLayer._controlled).length) canvas.activeLayer.releaseAll();
			event.data._selectState = 1;`
		);
	}
}

let tokenScene = (tokenName) => {
  for (let scene of game.scenes.entities) {
    let token = scene.data.tokens.find(t=> t.name === tokenName);
    if (token) return {scene, token};
  }
  return null;
}

// Hooks.on('init', Deselection.init);
/**
 * Application initialization
 */
Hooks.once("init", () => {
      /*
     * Key is the app name of the sheet.
     * value is the css class to identify the image/entry to attach the roll funtion to.
     *   It can be the image associated with the weapon/spell but does not need to be, anything unique will probably be fine.
     *   Hooks are added for rendering all of the sheets so you can have multiple ones active at the same time if you so wish.
     * */
 });
 
Hooks.once("setup", () => {
    MinorQOL = {
        checkSaves: checkSaves,
        resetStatus: resetStatus,
        doCombinedRoll: doCombinedRoll,
        doMacroRoll: doMacroRoll,
        doRoll: doRoll
    };
});

function requestPCSave(ability, playerId, actorId, advantage) {
 if (lmrtfyActive && playerRollSaves === "letme") {
    const socketData = {
        user: playerId,
        actors: [actorId],
        abilities: [],
        saves: [ability],
        skills: [],
        advantage: advantage ? 1 : 0,
        mode: "roll",
        title: "You need to save ...",
        message: "Please roll",
        formula: "",
        deathsave: false,
        initiative: false
    }
    game.socket.emit('module.lmrtfy', socketData);
 } else {
   let player = game.users.get(playerId);
   let actorName = game.actors.get(actorId).name;
  ChatMessage.create({
    content: ` ${actorName} Roll ${CONFIG.DND5E.abilities[ability]} saving throw${advantage ? " with advantage" : ""}`,
    whisper: [player]
  });
 }
}
